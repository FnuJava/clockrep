package com.paic.arch.interviews;

/**
 * 常量类
 * @author Administrator
 *
 */
public class TimeContants {
	
	public final static String  BASE_SHOW_FOUR = "OOOO";//四个灯暗显示 
	public final static String  BASE_SHOW_TWELVE = "YYRYYRYYRYY";//11个灯亮显示
	
	/**
	 * 灯颜色
	 * @author Administrator
	 *
	 */
	public enum LampColor{
		Y,
		R,
		O;
	}

}
